<?php session_start(); include("./securiteSimple.php") ;?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Application Projet Velo</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="style.css" type="text/css" />

</head>
<body>
<header class="header">
	<nav class="menu">
		<ul>
			<li class="entete"><a href="index.php">Page accueil</a>
			</li>
			<li class="entete"><a href="Creation.php">Creation</a>
			</li>
			<li class="liste"><a href="#">Informations</a>
			<ul class="submenu">
					<li><a href="Visualisation.php">Informations sur vos équipes</a></li>
					<li><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/index.php">Informations équipes Université de Lille</a></li>
					<li><a href="http://serveur-etu.polytech-lille.fr/~mbourqui/challengevelo/connexion.php">Informations personnelles</a></li>
			</ul>
			</li>
			<li class="entete"><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/gestionEquipe.php ">Gestion d'équipe</a>
			<li class="liste"><a href="#">Inscription</a>
			<ul class="submenu">
				<li><a href="http://serveur-etu.polytech-lille.fr/~mbourqui/challengevelo/">S'inscrire sur le site Challenge vélo Université de Lille</a></li>
				<li><a href="Inscription.php">S'inscrire à une équipe</a></li>
			</ul>
			<li class="entete"><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/consulterEquipes.php">Messagerie</a>
		</ul>
	</nav>
</header>
<br/><br/>
<a href="index.php">Page principale</a>
<?php 
$equipe=$_POST["equipe"];
$desc=$_POST["desc"];
$capi=$_SERVER['PHP_AUTH_USER'];
$db=connexionBase();
$check=checkDejaExistEquipe($db, $equipe);
if ($check==-1) {
	Creation($db,$equipe, $desc, $capi);
	?><h1 id="titre"> Vous avez bien créé votre équipe ! </h1>
<?php
}
  else {
  	?>
<h1 id="titre">Erreur ! l'équipe existe déjà </h1>
  <?php } ?>
</body>
</html>