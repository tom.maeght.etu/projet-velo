<?php session_start(); include("./securiteSimple.php") ;?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Application Vidéoclub</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="style.css" type="text/css" />

</head>
<body>
<header class="header">
	<nav class="menu">
		<ul>
			<li class="entete"><a href="index.php">Page accueil</a>
			</li>
			<li class="entete"><a href="Creation.php">Creation</a>
			</li>
			<li class="liste"><a href="#">Informations</a>
			<ul class="submenu">
					<li><a href="Visualisation.php">Informations sur vos équipes</a></li>
					<li><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/index.php">Informations équipes Université de Lille</a></li>
					<li><a href="http://serveur-etu.polytech-lille.fr/~mbourqui/challengevelo/connexion.php">Informations personnelles</a></li>
			</ul>
			</li>
			<li class="entete"><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/gestionEquipe.php ">Gestion d'équipe</a>
			<li class="liste"><a href="#">Inscription</a>
			<ul class="submenu">
				<li><a href="http://serveur-etu.polytech-lille.fr/~mbourqui/challengevelo/">S'inscrire sur le site Challenge vélo Université de Lille</a></li>
				<li><a href="Inscription.php">S'inscrire à une équipe</a></li>
			</ul>
			<li class="entete"><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/consulterEquipes.php">Messagerie</a>
		</ul>
	</nav>
</header>
<br/><br/>
<h1 align="center" id="titre">Informations sur les membres de l'équipe</h1>
<?php 
$team=$_POST["team"];

$uid=73749;

$db=connexionBase();
$membresNaviki=getMembres($uid, $isInsidePolytech=FALSE);




	///Requete pour avoir le nombre de membre de l'equipe
    $db=connexionBase();
	$requeteSQL="select count(refmembre) as nbmembre from membres where refequipe='$team';" ;
	$resNb=pg_query($db,$requeteSQL) ; $nbmembre=pg_fetch_assoc($resNb)["nbmembre"] ;
	?>
		
	<?php 
	//Requete qui permet d'avoir la liste des membres
	$requeteSQL2="select pseudo, email , fonction,libelle,uid from cycliste cy join campus cam on cy.refcampus=cam.code join membres m on m.refmembre=cy.email
	where refequipe='$team';" ;
	$result=pg_query($db,$requeteSQL2) ;
	$listemembre=pg_fetch_all($result);
	?>
	<?php 
	//Requete qui permet d'avoir les infos de l'equipe
	$requeteSQL3="select nom, descriptif,  refcapitaine from equipe where nom='$team';" ;
	$res=pg_query($db,$requeteSQL3) ;
	$infosequipe=pg_fetch_assoc($res);
	?>
 <table class="table table-bordered table-striped">
    <thead class="thead-dark"><tr><th>Pseudo</th><th>Email</th><th>Fonction</th><th>Campus</th><th>Total km</th></tr></thead>
    <tbody>
<?php
 $totalkm= 0;
    foreach($listemembre as $m) { 
      $infosMembre=getInfoMembreEquipe($uid,$m["uid"]);
      $totalkm+=$infosMembre["totalKm"];
    	?>
      <tr>
        <td><?= $m["pseudo"] ;?></td>
        <td><?= $m["email"] ;?></td>
        <td><?= $m["fonction"] ;?></td>
        <td><?= $m["libelle"] ;?> </td>
         <td><?= $infosMembre["totalKm"] ;?></td>
      </tr>
<?php
	 } // fin du foreach     
?>
    </tbody>
  </table>
  <?php
	//  $infoequipe=getInfoEquipe($uid,$isInsidePolytech = FALSE) ; 
?>
<h1 align="center" id="titre">Informations sur l'équipe</h1>
   <table class="table table-bordered table-striped">
    <thead class="thead-dark"><tr><th>Nom de l'équipe </th><th> Descriptif </th><th>Mail du capitaine</th><th>Effectif</th><th>Nombre total de km</th></tr></thead>
    <tbody>
    
      <tr>
        <td><?= $infosequipe["nom"] ;?></td>
        <td><?= $infosequipe["descriptif"] ;?></td>
        <td><?= $infosequipe["refcapitaine"] ;?></td>
        <td><?= $nbmembre;?></td>
        <td><?= $totalkm;?></td>
        
      </tr>

    </tbody>
  </table>


</body>
</html>