<?php
require ("./modele/openDataNaviki.php"); 

echo "<h1> Les équipes du Challenge Vélo </h1>";

$equipes=getEquipes() ;

foreach ($equipes as $nom => $uid) {
    print ("Equipe $nom : $uid<br />") ;
}

echo "<h1> L'équipe Université de Lille </h1>";
$infos=getInfoEquipe($equipes["Université de Lille"]);
if ($infos==-1) {
    print ("erreur accès naviki"); exit ;
}
print_r($infos);

echo "<h1> Les membres de Université de Lille </h1>";
$membres=getMembres($infos["uid"]) ;
foreach ($membres as $m) {
    $membre=getInfoMembreEquipe($infos["uid"],$m) ;
    echo "Nom : ".$membre["name"]."<br />" ;
}


?>