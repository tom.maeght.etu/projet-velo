
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Application Vidéoclub</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="Creationcss.css" type="text/css" />

<script type="text/javascript" src="./js/jquery-3.2.1.min.js" ></script>
<script type="text/javascript" src="./js/popper.js"></script>
<script src="./js/bootstrap.min.js" ></script>


</head>
<body>

<div id="retour_menu">
<a href="index.php">Page Principale</a>
</div>
<div class="form">
<form action="Visualisation.php" method="post">
<label> Votre mail: </label>
<input class="champ" type="text" placeholder="Veuillez rentrer votre adresse mail">
<br/>
<label> Votre pseudo: </label>
<input class="champ" type="text" placeholder="Veuillez rentrer votre pseudo">
<br/>
<label> Votre fonction: </label>
<input class="champ" type="text" placeholder="Veuillez rentrer votre fontion au sein de l'université">
<br/>
<br/>
<label> Voulez-vous recevoir une notification par mail de votre inscription ? : </label>
<select class="champ">
<option>Oui</option>
<option selected="selected">Non</option>
</select>
<br/>
<label> Votre mot de passe : </label>
<input class="champ" type="password" placeholder="Veuillez rentrer votre mot de passe (minimum 8 caractères)" minlength=8>

<br/>
<label> Selectionnez votre campus: </label>
<select class="champ">
<option>Santé</option>
<option>Cité scientifique</option>
</select>

<br/>
<input type="submit" class="bouton" value="S'inscrire">
</form>
</div>
</body>
</html>
