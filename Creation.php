<?php session_start(); include("./securiteSimple.php") ;?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Application Vidéoclub</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="style.css" type="text/css" />

</head>
<body>
<header class="header">
	<nav class="menu">
		<ul>
			<li class="entete"><a href="index.php">Page accueil</a>
			</li>
			<li class="entete"><a href="Creation.php">Creation</a>
			</li>
			<li class="liste"><a href="#">Informations</a>
			<ul class="submenu">
					<li><a href="Visualisation.php">Informations sur vos équipes</a></li>
					<li><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/index.php">Informations équipes Université de Lille</a></li>
					<li><a href="http://serveur-etu.polytech-lille.fr/~mbourqui/challengevelo/connexion.php">Informations personnelles</a></li>
			</ul>
			</li>
			<li class="entete"><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/gestionEquipe.php ">Gestion d'équipe</a>
			<li class="liste"><a href="#">Inscription</a>
			<ul class="submenu">
				<li><a href="http://serveur-etu.polytech-lille.fr/~mbourqui/challengevelo/">S'inscrire sur le site Challenge vélo Université de Lille</a></li>
				<li><a href="Inscription.php">S'inscrire à une équipe</a></li>
			</ul>
			<li class="entete"><a href="http://serveur-etu.polytech-lille.fr/~ljeronim/projet-bd/challengeVelo/consulterEquipes.php">Messagerie</a>
		</ul>
	</nav>
</header>

<?php 
	$base=connexionBase();
	$query="SELECT DISTINCT email FROM Cycliste ORDER BY email;";
	$resultcap=pg_query($base, $query);
?>
<div class="form">
	<form action="creation2.php" method="post">
		<label>Nom de l'équipe : </label> 
			<input name="equipe"class="champ" type="text" placeholder="Veuillez rentrer le nom de l'équipe">
		<br/>
		<label>Descriptif : </label>
			<input name="desc" class="champ" type="text" placeholder="Veuillez rentrer un descriptif pour l'équipe">
		<br/><br/>
		<div align="center">
		<input type="submit" class="bouton" value="Créer une équipe">
		</div>
	</form>
</div>	
</body>
</html>