<?php

// Connexion à la basede données velo

function connexionBase() {
	$server="localhost";
	$user="demo" ;
	$password="postgres" ;
	$base="velo" ;

	$base=pg_connect("host=$server dbname=$base user=$user password=$password")
	or die('Connexion impossible: '.pg_last_error()) ;
	return $base ;
}
//Fonctionnalité 3

function Creation($base,$equipe, $desc, $capi) {
	$requeteSQL="insert into equipe values('$equipe','$desc','$capi'); insert into membres values ('$capi','$equipe')";
	$result=pg_query($base,$requeteSQL) ;
	return pg_affected_rows($result) ;
}

//Fonctionnalité 4

function Inscription($base,$membre, $equipe) {
	$requeteSQL="insert into membres values('$membre','$equipe')" ;
	$result=pg_query($base,$requeteSQL) ;
	return pg_affected_rows($result)or die("Vous êtes déjà inscrit à cette equipe! <br/>") ;
}



// Cette fonction nous permet d'avoir la liste des équipes existantes
function getEquipe($base) {
	$requeteSQL="select nom from equipe" ;
	$resultat=pg_query($base,$requeteSQL)
	or die("Erreur SQL liste equipe!<br />$requeteSQL<br />") ;
	return pg_fetch_all($resultat) ;
}
//Cette fonction permet de verifier si le cycliste est deja inscrit
function checkDejaExistMembre($base,$eq,$inscrit) {
	$requeteSQL="select refmembre from membres where refequipe='$eq' 
	and refmembre='$inscrit' limit 1;" ; // on ne gère pas les homonymes
	$resultat=pg_query($base,$requeteSQL)
	or die("Erreur SQL existence membre !<br />$requeteSQL<br />") ;
	if (pg_num_rows($resultat)==0) return -1 ;
	$ligne=pg_fetch_assoc($resultat) ;
	return $ligne["refmembre"] ;
}

function checkAccesMembreExiste($base,$user,$pw) {
	$requeteSQL="select email from cycliste where email='$user'
	and password='$pw' limit 1;" ; // on ne gère pas les homonymes
	$resultat=pg_query($base,$requeteSQL)	or die("Erreur SQL existence équipe !<br />$requeteSQL<br />") ;
	if (pg_num_rows($resultat)==0) return -1 ;
	$ligne=pg_fetch_assoc($resultat) ;
	return $ligne["email"] ;
}


function checkDejaExistEquipe($base,$eq) {
	$requeteSQL="select nom from equipe where nom='$eq' limit 1;" ; // on ne gère pas les homonymes
	$resultat=pg_query($base,$requeteSQL)
	or die("Erreur SQL existence équipe !<br />$requeteSQL<br />") ;
	if (pg_num_rows($resultat)==0) return -1 ;
	$ligne=pg_fetch_assoc($resultat) ;
	return $ligne["nom"] ;
}
/**
 * @param $isInsidePolytech boolean : VRAI si utilisation de proxy polytech 
 *                                    pour accéder au service web
 * retourne la liste des équipes inscrites
 * retourne -1 si pb accès distant au service web
 * retourne un tableau associatif indicée par le nom de l'équipe. 
 * les valeurs du tableau sont les identifiant (uid) naviki des équipes.
 */
function getEquipes($isInsidePolytech = FALSE) {
    $urlServiceWebNaviki="https://www.naviki.org/naviki/api/v5/Contest/findTeams/51090?limit=500" ;
    $equipes=array();
    $serviceWebNaviki=curl_init() ;
    curl_setopt($serviceWebNaviki, CURLOPT_URL,$urlServiceWebNaviki) ;
    curl_setopt($serviceWebNaviki, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($serviceWebNaviki, CURLOPT_FOLLOWLOCATION, true);
    if ($isInsidePolytech) {
        curl_setopt($serviceWebNaviki,CURLOPT_PROXY,"http://proxy.polytech-lille.fr");
        curl_setopt($serviceWebNaviki, CURLOPT_PROXYPORT,3128);
    }
    $jsonCode = curl_exec($serviceWebNaviki);
    $httpCode=curl_getinfo($serviceWebNaviki,CURLINFO_HTTP_CODE);
    if ($httpCode != 200) return -1 ;
    $data=json_decode($jsonCode) ;
    foreach($data->teams as $team) $equipes[$team->name]=$team->uid;
    return $equipes;
}

/** 
 * @param $uidEquipe integer : identifiant Naviki d'une équipe
 * @param $isInsidePolytech boolean : VRAI si utilisation de proxy polytech 
 *                                    pour accéder au service web
 * retourne -1 si équipe non trouvée
 * retourne sinon un tableau associatif dont les entrées sont :
 * "uid", "nom", "totalKm", "kmParMembre"
 **/
function getInfoEquipe($uidEquipe,$isInsidePolytech = FALSE) {
    $urlServiceWebNaviki="https://www.naviki.org/naviki/api/v5/Contest/findTeams/51090?limit=500" ;
    $serviceWebNaviki=curl_init() ;
    curl_setopt($serviceWebNaviki, CURLOPT_URL,$urlServiceWebNaviki) ;
    curl_setopt($serviceWebNaviki, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($serviceWebNaviki, CURLOPT_FOLLOWLOCATION, true);
    if ($isInsidePolytech) {
      curl_setopt($serviceWebNaviki,CURLOPT_PROXY,"http://proxy.polytech-lille.fr");
      curl_setopt($serviceWebNaviki, CURLOPT_PROXYPORT,3128);
    }
    $jsonCode = curl_exec($serviceWebNaviki);
    $httpCode=curl_getinfo($serviceWebNaviki,CURLINFO_HTTP_CODE);
    if ($httpCode != 200) return -1 ;
    $data=json_decode($jsonCode) ;
    foreach($data->teams as $team) {
      if ($team->uid==$uidEquipe)
        return array("nom" => $team->name,
                     "uid" => $team->uid,
                     "totalKm" => $team->totalKm,
                     "kmParMembre" => $team->kmPerMember
                ) ;
    }
    return -1 ;
}

/** 
 * @param integer $uidEquipe : identifiant naviki d'une équipe
 * @param $isInsidePolytech boolean : VRAI si utilisation de proxy polytech 
 *                                    pour accéder au service web
 * retourne  un tableau associatif indicé par les pseudos des membres 
 *         et contenant les uid des membres
 */
function getMembres($uidEquipe, $isInsidePolytech=FALSE) {
    $urlServiceWebNaviki="https://www.naviki.org/naviki/api/v5/Contest/leaderboardOfMembersByTeam/$uidEquipe" ;
    $offset=0 ; $limit=100 ;
    $members=array() ;
    
    $serviceWebNaviki=curl_init() ;
    curl_setopt($serviceWebNaviki, CURLOPT_URL,$urlServiceWebNaviki) ;
    curl_setopt($serviceWebNaviki, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($serviceWebNaviki, CURLOPT_FOLLOWLOCATION, true);
    if ($isInsidePolytech) {
        curl_setopt($serviceWebNaviki,CURLOPT_PROXY,"http://proxy.polytech-lille.fr");
        curl_setopt($serviceWebNaviki, CURLOPT_PROXYPORT,3128);
    }
    
    do {
        $url=$urlServiceWebNaviki."?offset=$offset&limit=$limit";
        curl_setopt($serviceWebNaviki, CURLOPT_URL,$url) ;
        $jsonCode = curl_exec($serviceWebNaviki);
        $httpCode=curl_getinfo($serviceWebNaviki,CURLINFO_HTTP_CODE);
        if ($httpCode != 200) return -1 ;
        $data=json_decode($jsonCode) ;
        foreach($data->members as $membre) {
            $members[$membre->feUserName]=$membre->uid ;
        }
        $offset+=100 ;
    } while (count($data->members)==100) ;
    return $members;
}

/**
 * @param integer $uidEquipe : identifiant naviki d'une équipe
 * @param integer $uidMembre : identidiant naviki d'un membre
 * @param $isInsidePolytech boolean : VRAI si utilisation de proxy polytech
 *                                    pour accéder au service web
 * retourne  un tableau associatif qui décrit le membre trouvé,$this
 * les entrées du tableau sont uid, name, totalKm, rank
 * retourne -1 si pas trouvé
 */
function getInfoMembreEquipe($uidEquipe,$uidMembre, $isInsidePolytech=FALSE) {
    $urlServiceWebNaviki="https://www.naviki.org/naviki/api/v5/Contest/leaderboardOfMembersByTeam/$uidEquipe" ;
    $offset=0 ; $limit=100 ;
    
    $serviceWebNaviki=curl_init() ;
    curl_setopt($serviceWebNaviki, CURLOPT_URL,$urlServiceWebNaviki) ;
    curl_setopt($serviceWebNaviki, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($serviceWebNaviki, CURLOPT_FOLLOWLOCATION, true);
    if ($isInsidePolytech) {
        curl_setopt($serviceWebNaviki,CURLOPT_PROXY,"http://proxy.polytech-lille.fr");
        curl_setopt($serviceWebNaviki, CURLOPT_PROXYPORT,3128);
    }   
    do {
        $url=$urlServiceWebNaviki."?offset=$offset&limit=$limit";
        curl_setopt($serviceWebNaviki, CURLOPT_URL,$url) ;
        $jsonCode = curl_exec($serviceWebNaviki);
        $httpCode=curl_getinfo($serviceWebNaviki,CURLINFO_HTTP_CODE);
        if ($httpCode != 200) return -1 ;
        $data=json_decode($jsonCode) ;
        foreach($data->members as $membre) {
            if ($membre->uid==$uidMembre)
              return array("uid" => $membre->uid,
                           "name" =>$membre->feUserName,
                           "totalKm" => $membre->totalKm,
                           "rank" => $membre->rank
                          ) ;
        }
        $offset+=100 ;
    } while (count($data->members)==100) ;
    return -1 ;

}


?>